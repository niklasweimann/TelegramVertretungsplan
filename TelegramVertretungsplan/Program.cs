﻿using System;
using System.Threading.Tasks;
using NetTelegramBotApi;
using NetTelegramBotApi.Requests;
using System.Net;
using NetTelegramBotApi.Types;
using System.Linq;
using System.Reflection;

namespace TelegramVertretungsplan
{
    class Program
    {
        private static bool stopMe = false;

        static void Main(string[] args)
        {
            var botAPICode = "";
            var vPlanURL = "";
            if (System.IO.File.Exists(@"Config.ini"))
            {
                var ConfigINI = new IniFile(@"Config.ini");
                botAPICode = ConfigINI.Read("APIToken", "General");
                vPlanURL = ConfigINI.Read("UnitsURL", "General");
            }

            Console.WriteLine("Bot wird gestartet...");

            var t = Task.Run(() => RunBot(botAPICode, vPlanURL));

            Console.ReadLine();
            stopMe = true;
        }
        public static void RunBot(string accessToken, string vPlanURL)
        {
            var bot = new TelegramBot(accessToken);

            var me = bot.MakeRequestAsync(new GetMe()).Result;
            if (me == null)
            {
                Console.WriteLine("API Code nicht in Config gefunden");
                Console.WriteLine("(Drücke ENTER um zu beenden)");
                Console.ReadLine();
                return;
            }
            Console.WriteLine("Als \"{0} (@{1})\" verbunden!", me.FirstName, me.Username);

            Console.WriteLine();
            Console.WriteLine("Suche nach \"@{0}\" in Telegram und sende eine Nachricht.", me.Username);
            Console.WriteLine("(Drücke ENTER, um den Bot zu beenden.)");
            Console.WriteLine();


            long offset = 0;
            string uploadedPhotoId = null;
            while (!stopMe)
            {
                var updates = bot.MakeRequestAsync(new GetUpdates() { Offset = offset }).Result;
                if (updates != null)
                {
                    foreach (var update in updates)
                    {
                        offset = update.UpdateId + 1;
                        if (update.Message == null)
                        {
                            continue;
                        }
                        var from = update.Message.From;
                        var text = update.Message.Text;
                        var photos = update.Message.Photo;
                        Console.WriteLine(
                            "Nachricht von {0} {1} ({2}) um {4}: {3}",
                            from.FirstName,
                            from.LastName,
                            from.Username,
                            text,
                            update.Message.Date);

                        if (photos != null)
                        {
                            var webClient = new WebClient();
                            foreach (var photo in photos)
                            {
                                Console.WriteLine("  Neues Bild empfangen: Abmessungen {1}x{2} px, {3} bytes, id: {0}", photo.FileId, photo.Height, photo.Width, photo.FileSize);
                                var file = bot.MakeRequestAsync(new GetFile(photo.FileId)).Result;
                                var tempFileName = Path.GetTempFileName();
                                webClient.DownloadFile(file.FileDownloadUrl, tempFileName);
                                Console.WriteLine("    Gespeichert in {0}", tempFileName);
                            }
                        }

                        if (string.IsNullOrEmpty(text))
                        {
                            continue;
                        }
                        if (text == "/help")
                        {
                            var keyb = new ReplyKeyboardMarkup()
                            {
                                Keyboard = new[] {
                                    new[] { new KeyboardButton("/vPlan") },
                                    new[] { new KeyboardButton("/vPlanMorgen") },
                                    new[] { new KeyboardButton("/help") }
                                },
                                OneTimeKeyboard = true,
                                ResizeKeyboard = true
                            };
                            var reqAction = new SendMessage(update.Message.Chat.Id, "Hier sind alle möglichen Befehle:") { ReplyMarkup = keyb };
                            bot.MakeRequestAsync(reqAction).Wait();
                            continue;
                        }
                        if (text == "/vPlan")
                        {
                            try
                            {
                                bot.MakeRequestAsync(new SendMessage(
                                update.Message.Chat.Id,
                                VertretungsplanRequest.makevPlanrequest(vPlanURL))
                                {
                                    ParseMode = SendMessage.ParseModeEnum.HTML
                                }).Wait();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Nachricht konnte nicht gesendet werden. (" + e.InnerException + ")");
                            }
                            
                        }
                        if (text == "/vPlanMorgen")
                        {
                            try
                            {
                                bot.MakeRequestAsync(new SendMessage(
                                update.Message.Chat.Id,
                                VertretungsplanRequest.makevPlanrequestTomorrow(vPlanURL))
                                {
                                    ParseMode = SendMessage.ParseModeEnum.Markdown
                                }).Wait();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Nachricht konnte nicht gesendet werden. (" + e.InnerException + ")");
                            }

                        }
                        if(text == "/essensplan")
                        {
                            try
                            {
                                var cafeteriaPlan = "";
                                if (System.IO.File.Exists(@"Config.ini"))
                                {
                                    var ConfigINI = new IniFile(@"Config.ini");
                                    cafeteriaPlan = ConfigINI.Read("Speiseplan", "Cafeteria");
                                }
                                bot.MakeRequestAsync(new SendMessage(
                                update.Message.Chat.Id,
                                cafeteriaPlan
                                )
                                {
                                    ParseMode = SendMessage.ParseModeEnum.Markdown
                                }).Wait();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Nachricht konnte nicht gesendet werden. (" + e.InnerException + ")");
                            }
                        }
                    }
                }
            }
        }
    }
}
