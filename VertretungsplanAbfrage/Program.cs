﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;

namespace VertretungsplanAbfrage
{
    class Program
    {
        static void Main(string[] args)
        {
            TextWriter tw = new StreamWriter("date.txt");
            foreach (var item in StripHTML(getWebsite("https://www.stiftkeppel.schule/schule/content/w/12/w00000.htm")))
            {
                tw.WriteLine(item);
                Console.WriteLine(item);
            }
            Console.ReadKey();
            tw.Close();
        }
        public static string[] getWebsite(string pURL)
        {
            string mUrl = pURL;
            string quellCode;
            using (WebClient client = new WebClient())
            {
                quellCode = client.DownloadString(mUrl);
            }
            return Regex.Split(quellCode, "<.*?>");
        }
        public static string[] StripHTML(string[] pInput)
        {
            string[] mInput = pInput;
            List<string> lst = new List<string>();
            var values = new[] { "|", "&nbsp;", "Art","Datum","Std.", "Klasse(n)", "(Fach)", "Raum" };

            for (int i = 15; i < mInput.Length-7; i++)
            {
                mInput[i] = Regex.Replace(mInput[i], "<.*?>", String.Empty);
                mInput[i] = Regex.Replace(mInput[i], @"\[(.*?)\]", String.Empty);

                foreach (var item in values)
                {
                    if (mInput[i].Contains(item))
                        mInput[i] = string.Empty;
                }

                if (mInput[i] != String.Empty && mInput[i] != "" && mInput[i] != "\r" && mInput[i] != "\r\n")
                    lst.Add(mInput[i]);
            }
            return lst.ToArray();
        }
    }
}
