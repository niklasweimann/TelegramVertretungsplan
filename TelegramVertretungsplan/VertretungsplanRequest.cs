﻿using System;
using System.Data;
using System.Net;

namespace TelegramVertretungsplan
{
    public static class VertretungsplanRequest
    {
        public static string makevPlanrequest(string pURL)
        {
            return getVertretungsplan(0, pURL);
        }

        public static string makevPlanrequestTomorrow(string pURL)
        {
            return getVertretungsplan(1, pURL);
        }

        private static string getVertretungsplan(int pDay, string pURL)
        {
            string mVertretung = "";
            int mDay = pDay;
            var mDateValue = DateTime.Now.AddDays(mDay);
            //var mCurrentDay = mDateValue.Day.ToString("dddd");
            var mCurrentDay = "Mittwoch";
            if (mCurrentDay == "Montag")
            {
                mVertretung = extractVPlan(1, pURL);
            }
            else if (mCurrentDay == "Dienstag")
            {
                mVertretung = extractVPlan(2, pURL);
            }
            else if (mCurrentDay == "Mittwoch")
            {
                mVertretung = extractVPlan(3, pURL);
            }
            else if (mCurrentDay == "Donnerstag")
            {
                mVertretung = extractVPlan(4, pURL);
            }
            else if (mCurrentDay == "Freitag")
            {
                mVertretung = extractVPlan(5, pURL);
            }
            else
            {
                switch (mCurrentDay)
                {
                    case "Samstag":
                        mVertretung = "Es ist Wochende!!!! Zeit zu entspannen! Und die Hausaufgaben nicht vergessen. ;)";
                        break;
                    case "Sonntag":
                        mVertretung = "Sonntag!!! Wochenende";
                        break;
                    default:
                        break;
                }
            }
            return mVertretung;
        }

        private static string extractVPlan(int Day, string pURL)
        {
            var VPlanText = "";
            var websiteCode = getWebsite(pURL);
            VPlanText = makeVText(Day, websiteCode);
            return VPlanText;
        }

        private static string makeVText(int Day, string websiteCode)
        {
            var Text = "";
            var wCode = websiteCode;
            Text = getBetween(wCode, "<a name=\"" + Day + "\">", "\r\n<p>");

            var Data = HtmlTableParser.ParseHTML(Text);
            
            return ;
        }
        private static string getWebsite(string pURL)
        {
            string mUrl = pURL;
            string quellCode;
            using (WebClient client = new WebClient())
            {
                quellCode = client.DownloadString(mUrl);
            }
            return quellCode;
        }

        private static string getBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }
    }
}
